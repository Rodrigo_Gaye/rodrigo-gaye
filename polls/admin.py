from django.contrib import admin

from .models import Persona
from .models import Tarea


admin.site.register(Persona)
admin.site.register(Tarea)
