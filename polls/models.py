
import datetime

from django.db import models
from django.utils import timezone

class Persona(models.Model):
    nombre = models.CharField(max_length=25)
    email = models.EmailField()

    def __str__(self):
    	return self.nombre

class Tarea(models.Model):
    titulo = models.CharField(max_length=25)
    descripcion = models.CharField(max_length=200)
    persona_asignada = models.ForeignKey(Persona, on_delete=models.CASCADE)

    def __str__(self):
    	return self.titulo