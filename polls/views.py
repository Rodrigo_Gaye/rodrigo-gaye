from django.shortcuts import render, HttpResponse
from django.utils import timezone
from .models import Tarea


def home(request):
	lista_ultimas_tareas = list(Tarea.objects.all())
	return render(request, 'polls/index.html', {'lista_ultimas_tareas': lista_ultimas_tareas})